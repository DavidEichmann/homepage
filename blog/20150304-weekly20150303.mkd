---
author: thoughtpolice
title: "GHC Weekly News - 2015/03/03"
date: 2015-03-04T02:52:21
tags: ghc news
---

Hi \*,

It's that time again! Today GHC HQ met on a Tuesday to avoid some
scheduling conflicts, and that means it's time to send some news to
people.

Just a quick reminder from last week: we're hoping to make our third
GHC 7.10.1 release candidate on **Friday, March 13th**, with the
final release on **Friday, March 27th**.

Today, GHC HQ met up and mostly discussed the current status of GHC 7.10 and its bugs, which you can find on the Status page: <https://ghc.haskell.org/trac/ghc/wiki/Status/GHC-7.10.1>

But we've also had a little more list activity this week than we did before:

 - Simon PJ showed up to tell everyone he'd be a bit busy due to upcoming ICFP deadlines! However, since it's passed as of last Friday, it looks like the coming weeks will be more normal. <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008420.html>

 - Michael Snoyman started a thread about a serious bug that has eluded attention during the 7.10 RC period: `cabal haddock --hoogle` does not work! <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008412.html>

 - Jan Bracker is working on using the new type-checking plugins infrastructure, and had a question about the usage of what the typechecker calls `EvTerms`. Adam swooped in to respond. <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008414.html>

 - Ben Gamari has been working on #9661, but while trying to do so hit a very painful set of conditions due to the import dependency graph of the compiler, making his fix much more difficult. Simon responds with his thoughts. <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008432.html>

 - After returning from an ICFP-writing-induced hiatus, Simon alerted us to a new paper of his describing his new, GADT-aware pattern matching checker. As usual, he'd love comments! The implementation will hopefully land in HEAD soon. <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008437.html>

 - Nikita Karetnikov was kind enough to alert the list that he's gotten a nice, Nix/NixOS based solution to building GHC, which he's documented on the wiki. <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008445.html>

Some noteworthy commits that went into `ghc.git` in the past week include:

 - Commit aead01902e1c41e85b758dbafd15e60d08956374 - the `-fwarn-unused-binds` warning was split into 3 warnings, fixing #17 (one of our oldest open tickets).

 - Commit 5be8ed4da1963ed2d45a65fb61d761c977707cce - restores `integer-gmp` compatibility with GMP 4.x. This will be part of GHC 7.10.

 - Commit 31d4f2e9c89e22a91f98b4a4aa0f80af6b07b60f - `make test` in the top-level directory now works as expected.

 - Commit 5200bdeb26c5ec98739b14b10fc8907296bceeb9 - Replace Windows SEH handlers with VEH handlers, working uniformly across x86 and x86_64.

Closed tickets the past week include: #9586, #10122, #10026, #8896, #10090, #10123, #10128, #10025, #10024, #10125, #9994, #9962, #10103, #10112, #10122, #9901, #10130, #10129, #9044, #8342, #8780, #10003, #17, #2530, #8274, and #10107.
